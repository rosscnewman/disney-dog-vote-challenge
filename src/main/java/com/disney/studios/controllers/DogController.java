package com.disney.studios.controllers;

import java.security.InvalidParameterException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.disney.studios.domain.Dog;
import com.disney.studios.domain.dto.VoteDTO;
import com.disney.studios.domain.dto.VoteTranslator;
import com.disney.studios.services.DogService;
import com.disney.studios.services.VoteService;

@Controller
@RequestMapping("/dogs")
public class DogController {
	@Autowired
	private DogService dogService;
	
	@Autowired
	private VoteService voteService;
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Iterable<Dog> list(@RequestParam(value="breed", required=false) String breed)
	{
		if (StringUtils.isEmpty(breed))
			return dogService.listAllDogs();
		return dogService.listDogsByBreed(breed);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Dog getById(@PathVariable Integer id)
	{
		return dogService.getDogByDogId(id);		
	}
	
	@RequestMapping(value = "/{id}/vote", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public @ResponseBody void addVote(@PathVariable Integer id, @RequestBody(required=true) VoteDTO vote)
	{
		if (vote.getVoteDirection() == null)
			throw new InvalidParameterException();
		vote.setDogId(id);
		voteService.addVoteToDog(VoteTranslator.translateDTOtoDAO(vote));
	}
	
	@RequestMapping(value = "/{id}/vote", method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
	public @ResponseBody void vote(@PathVariable Integer id, @RequestBody(required=true) VoteDTO vote)
	{
		vote.setDogId(id);
		voteService.removeVoteFromDog(VoteTranslator.translateDTOtoDAO(vote));
	}
	
}
