package com.disney.studios.services;

import com.disney.studios.domain.Vote;

public interface VoteService {
	Iterable<Vote> getVotesByDog(Integer dogId);
	Iterable<Vote> getVotesByUser(Integer userId);

	void addVoteToDog(Vote vote);
	void removeVoteFromDog(Vote vote);
}
