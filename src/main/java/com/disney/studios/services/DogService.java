package com.disney.studios.services;

import com.disney.studios.domain.Dog;

public interface DogService {
	Iterable<Dog> listAllDogs();
	Iterable<Dog> listDogsByBreed(String breed);
	Dog getDogByDogId(Integer id);
}
