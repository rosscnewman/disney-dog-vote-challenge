package com.disney.studios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.disney.studios.domain.Vote;
import com.disney.studios.repositories.VoteRepository;

@Service
public class VoteServiceImpl implements VoteService{
	@Autowired
	private VoteRepository voteRepository;
	
	@Override
	public Iterable<Vote> getVotesByDog(Integer dogId) {
		return null;
//		return voteRepository.findByVotePKDogId(dogId);
	}

	@Override
	public Iterable<Vote> getVotesByUser(Integer userId) {
		return null;
//		return voteRepository.findByUserId(userId);
	}

	@Override
	public void addVoteToDog(Vote vote) {
		voteRepository.save(vote);
	}

	@Override
	public void removeVoteFromDog(Vote vote) {
		voteRepository.delete(vote);
	}



}
