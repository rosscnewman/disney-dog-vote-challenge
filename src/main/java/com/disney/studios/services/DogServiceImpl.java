package com.disney.studios.services;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.disney.studios.domain.Dog;
import com.disney.studios.repositories.DogRepository;


@Service
public class DogServiceImpl implements DogService {
	
	@Autowired 
	private DogRepository dogRepository;
		
	@Override
	public List<Dog> listAllDogs() {
		Sort sort = new Sort(new Order(Direction.ASC, "breed"));
		Iterable<Dog> dogs = dogRepository.findAll(sort);
		
		Comparator<Dog> comparator = Comparator.comparing(Dog::getBreed);
		comparator = comparator.thenComparing(Comparator.comparing(Dog::getVoteCount).reversed());
		return StreamSupport.stream(dogs.spliterator(), false).sorted(comparator).collect(Collectors.toList());
	}

	@Override
	public List<Dog> listDogsByBreed(String breed) {
		Iterable<Dog> dogs = dogRepository.findByBreedIgnoreCase(breed);
		Comparator<Dog> comparator = Comparator.comparing(Dog::getVoteCount).reversed();
		
		return StreamSupport.stream(dogs.spliterator(), false).sorted(comparator).collect(Collectors.toList());
	}

	@Override
	public Dog getDogByDogId(Integer id) {
		return dogRepository.findOne(id);
	}


}
