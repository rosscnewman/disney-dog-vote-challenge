package com.disney.studios.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.PrePersist;
import javax.persistence.Table;


@Entity
@Table(name="votes")
public class Vote implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private VotePK votePK;
	
	@MapsId("dog")
	@ManyToOne
	@JoinColumn(name = "dog_id", referencedColumnName="dog_id")
	private Dog Dog;
	
	@Enumerated(EnumType.STRING)
	private VoteDirection votedirection;
	
	private Date timeVoted;
	
	@PrePersist
	protected void onCreate()
	{
		timeVoted = new Date();
	}

	public VotePK getVotePK() {
		return votePK;
	}
	
	public void setVotePK(VotePK votePK)
	{
		this.votePK = votePK;
	}
	
	public Date getTimeVoted() {
		return timeVoted;
	}
	public void setTimeVoted(Date timeVoted) {
		this.timeVoted = timeVoted;
	}

	public VoteDirection getVotedirection() {
		return votedirection;
	}

	public void setVotedirection(VoteDirection votedirection) {
		this.votedirection = votedirection;
	}
}
