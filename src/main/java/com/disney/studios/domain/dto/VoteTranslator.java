package com.disney.studios.domain.dto;

import com.disney.studios.domain.Vote;
import com.disney.studios.domain.VotePK;

public class VoteTranslator {
	public static Vote translateDTOtoDAO(VoteDTO dto)
	{
		Vote dao = new Vote();
		dao.setVotedirection(dto.getVoteDirection());
		VotePK pk = new VotePK();
		pk.setDogId(dto.getDogId());
		pk.setUserId(dto.getUserId());
		dao.setVotePK(pk);
		return dao;
	}
	
	public static VoteDTO translateDAOtoDTO(Vote dao)
	{
		//unimplemented
		return new VoteDTO();
	}
}
