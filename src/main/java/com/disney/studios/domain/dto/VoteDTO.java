package com.disney.studios.domain.dto;

import com.disney.studios.domain.VoteDirection;

public class VoteDTO {

	private Integer dogId;
	private Integer userId;
	private VoteDirection voteDirection;
	public Integer getDogId() {
		return dogId;
	}
	public void setDogId(Integer dogId) {
		this.dogId = dogId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public VoteDirection getVoteDirection() {
		return voteDirection;
	}
	public void setVoteDirection(VoteDirection voteDirection) {
		this.voteDirection = voteDirection;
	}
}
