package com.disney.studios.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;


@Embeddable 
public class VotePK implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Integer user_id;
	private Integer dog_id;		
	
	public VotePK() {}
	
	public VotePK(Integer userId, Integer dogId)
	{
		this.user_id = userId;
		this.dog_id = dogId;
	}
	
	public Integer getUserId()
	{
		return user_id;
	}
	
	public Integer getDogId()
	{
		return dog_id;
	}
	
	public void setDogId(Integer dog_id)
	{
		this.dog_id = dog_id;
	}
	
	public void setUserId(Integer user_id)
	{
		this.user_id = user_id;
	}
	
	
	
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof VotePK)) return false;
		return Objects.equals(getUserId(), ((VotePK) o).getUserId()) && 
				Objects.equals(getDogId(), ((VotePK) o).getDogId());
	}
	@Override
	public int hashCode()
	{
		return Objects.hash(getUserId(), getDogId());
	}
}