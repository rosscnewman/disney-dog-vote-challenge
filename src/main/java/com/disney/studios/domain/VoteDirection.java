package com.disney.studios.domain;

public enum VoteDirection {
	UP, DOWN
}
