package com.disney.studios.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="dogs")
@JsonInclude(Include.NON_NULL)
public class Dog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer dog_id;

	private String url;
	private String breed;

	@Formula("(SELECT (SELECT count(*) from votes v where v.dog_id = dog_id and v.votedirection = 'UP') - (SELECT count(*) from votes v where v.dog_id = dog_id and v.votedirection = 'DOWN' ))")
	private Integer voteCount;
	
	@Transient
	private Integer favoriteCount;
	
	public Integer getDogId()
	{
		return dog_id;
	}
	
	public void setDogId(Integer id)
	{
		this.dog_id = id;
	}
	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getBreed() 
	{
		return breed;
	}

	public void setBreed(String breed) 
	{
		this.breed = breed;
	}
	
	public void setVoteCount(Integer count)
	{
		this.voteCount = count;
	}
	public Integer getVoteCount()
	{
		return voteCount;
	}
	public void setFavoriteCount(Integer count)
	{
		this.favoriteCount = count;
	}
	public Integer getFavoriteCount()
	{
		return favoriteCount;
	}
}
