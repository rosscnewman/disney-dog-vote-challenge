package com.disney.studios.repositories;

import org.springframework.data.repository.CrudRepository;

import com.disney.studios.domain.Vote;

public interface VoteRepository extends CrudRepository<Vote, Integer>{
//	Iterable <Vote> findByVotePKDogId(Integer dogId);
//	Iterable <Vote> findByVotePKUserId(Integer userId);
}
