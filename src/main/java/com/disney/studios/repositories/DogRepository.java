package com.disney.studios.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.disney.studios.domain.Dog;

public interface DogRepository extends PagingAndSortingRepository<Dog, Integer> {
	Iterable <Dog> findByBreedIgnoreCase(String breed);
}
