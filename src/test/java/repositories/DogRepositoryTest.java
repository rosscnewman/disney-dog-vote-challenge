package repositories;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.disney.studios.domain.Dog;
import com.disney.studios.domain.Vote;
import com.disney.studios.domain.VoteDirection;
import com.disney.studios.domain.dto.VoteDTO;
import com.disney.studios.domain.dto.VoteTranslator;
import com.disney.studios.repositories.DogRepository;
import com.disney.studios.repositories.VoteRepository;

import configuration.RepositoryConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {RepositoryConfiguration.class})
public class DogRepositoryTest {
	@Autowired
	private DogRepository dogRepository;
	
	@Autowired
	private VoteRepository voteRepository;
		
	private Dog dog;
	
	@Before
	public void before()
	{
		voteRepository.deleteAll();
		dogRepository.deleteAll();
		dog = new Dog();
		dog.setBreed("Labrador");
		dog.setUrl("http://url.com");

		dogRepository.save(dog);
		dog = dogRepository.findOne(dog.getDogId());
	}
	
	
	
	@Test
	public void testVoteDog()
	{
		assertNotNull(dog.getDogId());
		
		VoteDTO voteDTO = new VoteDTO();
		voteDTO.setDogId(dog.getDogId());
		voteDTO.setUserId(1);
		voteDTO.setVoteDirection(VoteDirection.UP);
		
		Vote voteDAO = VoteTranslator.translateDTOtoDAO(voteDTO);
		
		assertEquals(voteDTO.getDogId(), voteDAO.getVotePK().getDogId());
		assertEquals(voteDTO.getUserId(), voteDAO.getVotePK().getUserId());
		assertNull(voteDAO.getTimeVoted());
		
		voteRepository.save(voteDAO);
		
		Iterable<Vote> dbVotes = voteRepository.findAll();
		
		int count = 0;
		for(Vote dbVote : dbVotes)
		{
			count++;
			assertNotNull(dbVote.getTimeVoted());
		}
		assertEquals(count, 1);
	}
	
	@Test
	public void testSaveDog()
	{
		Dog dbDog = dogRepository.findOne(dog.getDogId());
		
		assertNotNull(dbDog);
		
		assertEquals(dog.getDogId(), dbDog.getDogId());
		assertEquals(dog.getBreed(), dbDog.getBreed());
		
		long dogCount = dogRepository.count();
		assertEquals(dogCount, 1);
	}
	
}
