# API Challenge

## Dependencies

This project uses Maven for builds.
You need Java 8 installed.

## Building

```
$ mvn package
```

## Running

```
$ java -jar target/api_interview-0.1.0.jar
```
Dog Response Body:
{
    "url": "http://i.imgur.com/eE29vX4.png",
    "breed": "Labrador",
    "voteCount": 0, //Calculated by (Upvotes - Downvotes) for a specific dog
    "dogId": 1
}
API routes:
GET localhost:8080/dogs
Returns all dogs, ordered by Breed.

GET localhost:8080/dogs?breed=<breed>
Returns all dogs from a specific breed. (case-insensitive)

GET localhost:8080/dogs/<id>
Returns full details of a dog with id <id>

POST localhost:8080/dogs/<dogId>/vote
RequestBody
{
	"userId": 1,
	"voteDirection": "UP" //"UP" or "DOWN" are valid 
}
Adds one upvote or downvote to dog. User can vote on dog once and change vote direction. Does not enforce logged in user

DELETE localhost:8080/dogs/<dogId>/vote
RequestBody
{
	"userId": 1, 
}
Removes vote from dog for user. Does not enforce logged in user.